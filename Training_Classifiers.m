clear ; close all; rng('default');
% ..................Load Training Data.......................
data= importdata('C:\Users\Documents\dataset3.txt');
%............Load Testing data..............................
testdata=importdata('C:\Users\Documents\testdata.txt');
%.....Put all the columns  except the last one (label col.) in matrix.......
x_test=testdata(:,1:end-1);
X_data=data(:,1:end-1);
%...put the end column(labels) in vector....... 
y_test=testdata(:,end);
Y_data=data(:,end);
%%
%...............Classifiers....................................................................
%%
%...................SVM..........................................

%Train SVM Model
mdlSVM =fitcsvm(X_data,Y_data,'KernelFunction','rbf');
%Cross validate SVM
cvmodelSVM = crossval(mdlSVM);
%use predict function to get the accuracy and score of the classifier after cross validating  
[labelSVM,scoreSVM,costSVM] = kfoldPredict(cvmodelSVM);
%get TP,TF,FP,FN,TPR,FPR,TNR,FNR of SVM
[TNRSVM,FNRSVM,TPRSVM,FPRSVM,TPSVM, FPSVM, TNSVM, FNSVM] = calError(Y_data, labelSVM);

%kfoldLoss(obj) returns loss obtained by cross-validated classification model obj. 
%For every fold, this method computes classification loss for in-fold observations using a model
% trained on out-of-fold observations.
ErrorSVM = kfoldLoss(cvmodelSVM);
%Accuracy of trained model
AccuarcySVM=1-ErrorSVM;
%get perfomance of classifier (Accuracy,TPR,FPR,etc) this for make sure
%that othr calculation is correct
CPSVM = classperf(Y_data, labelSVM)
%get false and true postive rate,area under the curve   
[Xsvm,Ysvm,Tsvm,AUCsvm] = perfcurve(Y_data,scoreSVM(:,2),'1');

 

%.....................Knn Classifier......................

%train the model
%k=3
mdlKnn = fitcknn(X_data,Y_data,'NumNeighbors',3);
cvmodelKnn = crossval(mdlKnn);
ErrorKnn = kfoldLoss(cvmodelKnn);
AccuarcyKnn=1-ErrorKnn;
[labelKnn,scoreKnn,costKnn] = kfoldPredict(cvmodelKnn);
%get TP,TF,FP,FN,TPR,FPR,TNR,FNR of KNN
[TNRKnn,FNRKnn,TPRKnn,FPRKnn,TPKnn, FPKnn, TNKnn, FNKnn] = calError(Y_data, labelKnn);
[XKnn,YKnn,TKnn,AUCKnn] = perfcurve(Y_data,scoreKnn(:,2),'1');

%%
%...................NN Classifier.........

%k=1 
mdlNN = fitcknn(X_data,Y_data,'NumNeighbors',1);
cvmodelNN = crossval(mdlNN);
ErrorNN = kfoldLoss(cvmodelNN);
AccuarcyNN=1-ErrorNN;
[labelNN,scoreNN,costNN] = kfoldPredict(cvmodelNN);
%get TP,TF,FP,FN,TPR,FPR,TNR,FNR of NN
[TNRNN,FNRNN,TPRNN,FPRNN,TPNN, FPNN, TNNN, FNNN] = calError(Y_data, labelNN);
[XNN,YNN,TNN,AUCNN] = perfcurve(Y_data,scoreNN(:,2),'1');

%%
%..................RandomForest Classifier...................

%Method=Bag
%Create a bagged classification ensemble of 20 trees from the training data:
%produce/ensemble bag of trees
MdlRandomforest = fitensemble(X_data,Y_data,'Bag',100,'Tree','Type','Classification');
cvmodelRandomForest = crossval(MdlRandomforest);
ErrorRandomforest = kfoldLoss(cvmodelRandomForest);
AccuarcyRandomforest=1-ErrorRandomforest;
[labelRandomForest,scoreRandomForest,costRandomForest] = kfoldPredict(cvmodelRandomForest);
CPSRF = classperf(Y_data, labelRandomForest)
[TNRRF,FNRRF,TPRRF,FPRRF,TPRF, FPRF, TNRF, FNRF] = calError(Y_data, labelRandomForest);
[XRandomForest,YRandomForest,TRandomForest,AUCRandomForest] = perfcurve(Y_data,scoreRandomForest(:,2),'1');
%%

%...............................................Drawing Roc Curve........................................
plot(Xsvm,Ysvm);
hold on

 xlabel('False positive rate'); ylabel('True positive rate');
 title('ROC Curves for Decision Tree, knn, SVM,  Naive Bayes and Random Forest Classification');
plot(XTree,YTree);

 plot(XKnn,YKnn);
 plot(XRandomForest,YRandomForest);
 plot(Xnb,Ynb);
legend('SVM','DecisionTree','Knn','RandomForest','NB','Location','Best');
 hold off
%%
%................Display the results on console of matlab..................
 fprintf('Auc of Randomforest %3.4f%%\n',AUCRandomForest);
fprintf('AUC of knn %3.4f%%\n',AUCKnn);
fprintf('AUC of NN %3.4f%%\n',AUCNN);
fprintf('AUC of SVM %3.4f%%\n',AUCsvm);


 fprintf('Accuracy of Randomforest %3.4f%%\n',AccuarcyRandomforest);
fprintf('Accuracy  of knn %3.4f%%\n',AccuarcyKnn);
fprintf('Accuracy  of NN %3.4f%%\n',AccuarcyNN);
fprintf('Accuracy  of SVM %3.4f%%\n',AccuarcySVM);

fprintf(' FNR %3.4f%% ,TNR %3.4f%%, TPR %3.4f%%  & FPR %3.4f%% of Randomforest  \n',FNRRF,TNRRF,TPRRF,FPRRF);
fprintf('FNR %3.4f%% ,TNR %3.4f%%, TPR %3.4f%%  & FPR %3.4f%% of Knn \n',FNRKnn,TNRKnn,TPRKnn,FPRKnn);
fprintf('FNR %3.4f%% ,TNR %3.4f%%, TPR %3.4f%%  & FPR %3.4f%% of NN  \n',FNRNN,TNRNN,TPRNN,FPRNN);
fprintf('FNR %3.4f%% ,TNR %3.4f%%, TPR %3.4f%%  & FPR %3.4f%% of SVM  \n',FNRSVM,TNRSVM,TPRSVM,FPRSVM);
