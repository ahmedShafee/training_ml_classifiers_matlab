# Training_ML_Classifiers_Matlab

The training process of K-nearest neighbors (KNN) Machine learning (ML) algorithm, support vector machine ML algorithm, Random Forest (RF) ML algorithm using matlab algorithm.
In addition the evaluation process is done to the three classifiers using 10-fold cross-validation (CV) and the receiver operating characteristic curve (RoC) curve.
